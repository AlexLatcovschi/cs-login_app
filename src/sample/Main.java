package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Main extends Application {
    List<User> jsonUsers = new ArrayList<>();

    @Override
    public void start(Stage stage) throws Exception {
        getStageData(stage);
        getJsonUsers();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void getStageData(Stage stage) {
        stage.setTitle("Login App");
        final Group rootGroup = new Group();

        Text title = new Text("Please select option!");
        title.setLayoutX(20);
        title.setLayoutY(50);
        Button login = new Button("Login");
        login.setLayoutX(20);
        login.setLayoutY(100);
        login.setOnAction(value -> {
            login();
        });
        Button register = new Button("Register");
        register.setLayoutX(110);
        register.setLayoutY(100);
        register.setOnAction(value -> {
            try {
                register();
            } catch (Exception e) {
                System.out.println(e);
            }
        });
        rootGroup.getChildren().add(title);
        rootGroup.getChildren().add(login);
        rootGroup.getChildren().add(register);

        final Scene scene =
                new Scene(rootGroup, 1440, 700, Color.BEIGE);
        stage.setScene(scene);
        stage.show();
    }

    private void register() throws Exception {
        Stage stage = new Stage();
        final Group rootGroup = new Group();
        Text title = new Text("Register");
        title.setLayoutX(20);
        title.setLayoutY(50);
        rootGroup.getChildren().add(title);

        TextField name = new TextField();
        name.setPromptText("Name");
        name.setLayoutX(20);
        name.setLayoutY(100);
        rootGroup.getChildren().add(name);

        TextField surname = new TextField();
        surname.setPromptText("Surname");
        surname.setLayoutX(20);
        surname.setLayoutY(150);
        rootGroup.getChildren().add(surname);

        TextField email = new TextField();
        email.setPromptText("Email");
        email.setLayoutX(20);
        email.setLayoutY(200);
        rootGroup.getChildren().add(email);

        Button register = new Button("Register");
        register.setLayoutX(20);
        register.setLayoutY(250);
        register.setOnAction(value -> {
            String generatedPassword = generatePassword(6);
            try {
                sendMail(email.getText(), generatedPassword);
            } catch (Exception e) {
                System.out.println(e);
            }
            saveUser(new User(email.getText(), generatedPassword, name.getText(), surname.getText()));
            stage.close();
        });
        rootGroup.getChildren().add(register);

        final Scene scene =
                new Scene(rootGroup, 1440, 700, Color.BEIGE);
        stage.setScene(scene);
        stage.show();
    }

    private void login() {
        Stage stage = new Stage();
        final Group rootGroup = new Group();
        Text title = new Text("Login");
        title.setLayoutX(20);
        title.setLayoutY(50);
        TextField email = new TextField();
        email.setPromptText("Email");
        email.setLayoutX(20);
        email.setLayoutY(100);
        rootGroup.getChildren().add(title);
        rootGroup.getChildren().add(email);
        TextField password = new TextField();
        password.setPromptText("Password");
        password.setLayoutX(20);
        password.setLayoutY(150);
        rootGroup.getChildren().add(password);
        Button login = new Button("Login");
        login.setLayoutX(20);
        login.setLayoutY(200);
        login.setOnAction(value -> {
            boolean userExists = checkUserExists(email.getText(), password.getText());
            if (userExists) {
                stage.close();
            }
        });
        rootGroup.getChildren().add(login);
        final Scene scene =
                new Scene(rootGroup, 1440, 700, Color.BEIGE);
        stage.setScene(scene);
        stage.show();
    }

    public static void sendMail(String recepient, String mess) throws Exception {
        System.out.println("Preparing to send email");
        Properties properties = new Properties();

        //Enable authentication
        properties.put("mail.smtp.auth", "true");
        //Set TLS encryption enabled
        properties.put("mail.smtp.starttls.enable", "true");
        //Set SMTP host
        properties.put("mail.smtp.host", "smtp.gmail.com");
//        properties.put("mail.debug", "true");

        //Set smtp port
        properties.put("mail.smtp.port", "587");

        //Your gmail address
        String myAccountEmail = "alexandru.latcovschi.labs@gmail.com";
        //Your gmail password
        String password = "admin123ADMIN!@#";

        //Create a session with account credentials
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });

        //Prepare email message
        Message message = prepareMessage(session, myAccountEmail, recepient, mess);

        //Send mail
        Transport transport = session.getTransport("smtp");

        transport.send(message);
        System.out.println("Message sent successfully");
    }

    private static Message prepareMessage(Session session, String myAccountEmail, String recepient, String mess) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
            message.setSubject("My First Email from Java App");
            String htmlCode = "<h1> Your password for LOGIN_APP is: </h1> <br/> <h2><b>" +
                    mess + "</b></h2>";
            message.setContent(htmlCode, "text/html");
            return message;
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return null;
    }

    public void saveUser(User user) {
        JSONArray usersList = new JSONArray();
        jsonUsers.add(user);
        for (int i = 0; i < jsonUsers.size(); i++) {
            JSONObject userDetails = new JSONObject();
            userDetails.put("firstName", jsonUsers.get(i).name);
            userDetails.put("lastName", jsonUsers.get(i).surname);
            userDetails.put("email", jsonUsers.get(i).email);
            userDetails.put("password", jsonUsers.get(i).password);

            JSONObject userObject = new JSONObject();
            userObject.put("user", userDetails);
            usersList.add(userObject);
        }


        //Write JSON file
        try (FileWriter file = new FileWriter("users.json")) {
            file.write(usersList.toJSONString());
            file.flush();
            messageWindow("User created with success! Check email for password!", "success");
        } catch (IOException e) {
            messageWindow("Something went wrong", "error");
            e.printStackTrace();
        }
    }

    public void messageWindow(String message, String type) {
        Stage stage = new Stage();
        final Group rootGroup = new Group();
        Text title = new Text(message);
        title.setLayoutX(20);
        title.setLayoutY(50);
        if (type == "error") {
            title.setFill(Color.RED);
        } else {
            title.setFill(Color.BLUE);
        }

        rootGroup.getChildren().add(title);
        final Scene scene =
                new Scene(rootGroup, 1440, 700, Color.BEIGE);
        stage.setScene(scene);
        stage.show();
    }

    public void getJsonUsers() {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("users.json")) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray usersList = (JSONArray) obj;
            System.out.println(usersList);

            //Iterate over employee array
            usersList.forEach(emp -> parseUserObject((JSONObject) emp));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseUserObject(JSONObject user) {
        JSONObject employeeObject = (JSONObject) user.get("user");

        String firstName = (String) employeeObject.get("firstName");
        System.out.println(firstName);

        String lastName = (String) employeeObject.get("lastName");
        System.out.println(lastName);

        String email = (String) employeeObject.get("email");
        System.out.println(email);

        String password = (String) employeeObject.get("password");
        System.out.println(password);

        jsonUsers.add(new User(email, password, firstName, lastName));
    }

    private boolean checkUserExists(String email, String password) {
        boolean userExists = false;
        for (int i = 0; i < jsonUsers.size(); i++) {
            if (jsonUsers.get(i).email.equals(email)) {
                userExists = true;
                if (jsonUsers.get(i).password.equals(password)) {
                    messageWindow("You have successfully logged in!", "success");
                    return true;
                }
            }
        }
        if (!userExists) {
            messageWindow("Incorrect login data!", "error");
        }
        return false;
    }

    public String generatePassword(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }
}


