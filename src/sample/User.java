package sample;

public class User {
    public String email, password;
    public String surname, name;

    public User(String email, String password, String name, String surname) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }
}